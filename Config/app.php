<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(
	/**
	 * 短信配置
	 */
    'messageConfig' => array(
		//短信平台账号
    	'msg_uid' => '304056',
		//短信平台密码
  		'msg_pwd' => 'MY304056',
		//绑定ip需要联系客服
  		'msg_ip' => '112.74.129.90',
		//绑定端口需要联系客服
  		'msg_port' => '9890',
		//平台长号由客服生成
  		'msg_srcphone' => '106900846807',
		//刷新频率 默认5min
  		'msg_refresh_second' => 360,
		//短信发送模板
  		'msg_tpl' => '【米柚科技】您的验证码是',
    ),

	/**
	 * 基本设置
	 */
	'baseConfig' => array(
		//开放注册 0关闭 1开始 用于维护
			'open_register' => 1,


		//每篇文章分享并有效阅读默认获得2分钱
			'article_read_money' => 0.02,
		//每篇文章分享获得收益
			'article_share_money' => 0.02,
		//文章列表页数
			'article_display_number_single_page' => 5,

		//用户注册赠送金额
			'reward_money_by_register' => 1.00,
		//每天登陆赠送金额
			'reward_money_by_login' => 1.00,
		//邀请好友获得金额
			'reward_money_by_invite' => 5.00,
		//邀请好友师傅或者提成 （）%
			'rate' => 25,
	),
	/**
	 * 云上传引擎,支持local,oss,upyun
	 */
		'UCloudEngine' => 'local',

	/**
	 * 本地存储相关配置（UCloudEngine为local时的配置）
	 */
		'UCloud' => array(
			//对应的文件路径
				'host' => 'http://localhost/PhalApi/Public/upload'
		),

		'xxkpConfig' => array(
		//签名信息
			's' => '12345678'
		)
);
