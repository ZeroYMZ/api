<?php
session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <title><?php echo $data['title'] ?></title>
    <meta name="keywords" content="<?php echo $data['title'] ?>"/>
    <meta name="description" content="<?php echo $data['title'] ?>"/>
    <link rel="stylesheet" type="text/css" href="static/hz.css" media="all">
    <div style="height:0;overflow:hidden;"><img
            src="http://img01.store.sogou.com/net/a/04/link?appid=100520031&w=710&url=<?php echo $_pic ?>"></div>
</head>
<body class="mhome" style="font-family:'STXihei';" data-xthreshold="200">
<!-- 悬浮小二维码 -->
<div class="code-small" id="code-small">
    <div class="code-small-img"></div>
    <div class="code-small-font">送红包</div>
</div>
<!-- 悬浮大二维码 -->
<div class="code-big code-big-right">
    <img class="code-big-img">

    <div class="code-big-font">长按二维码，关注公众号，注册领红包！</div>
</div>
<!-- 返回按钮 -->
<div class="article">
    <h1 class="title" style="font-family: '微软雅黑';"><?php echo $data['title'] ?></h1>

    <div class="article-df">
        <p class="article-d"><?php echo $data['day'] ?></p>

        <p class="article-f"><a
                href="http://mp.weixin.qq.com/s?__biz=MzI1MzE1MzUwMg==&mid=402827463&idx=1&sn=695b31c3680c219d89907114ccde9e94#rd"
                style="color:#607fa6">有条资讯</a></p>

        <p class="article-d">阅读&nbsp<?php echo $data['pv'] ?></p>
    </div>
    <div class="rich_media_content" id="js_content">
        <!--内容-->
        <div id="neirong" class="unopen" style="font-family: '微软雅黑';">

            <?php echo $data['content'] ?>
        </div>
        <div class="open" id="open">
            <div class="openfont">展开全文</div>
        </div>
        <div class="ad_display">
            <div class="list">
                <div class="tjread">
                    <div class="tj-logo"></div>
                    <div class="tj-font">推荐阅读</div>
                </div>
                <script type="text/javascript" src="http://s.csbew.com/k.js"></script>
                <!-- 122887：资讯文章1 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122887, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122897：资讯广告位1 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122897, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122889：资讯文章2 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122889, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122899：资讯广告位2 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122899, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122891：资讯文章3 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122891, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122901：资讯广告位3 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122901, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122893：资讯文章4 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122893, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122903：资讯广告位4 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122903, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122895：资讯文章5 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122895, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>
                <!-- 122905：资讯广告位5 类型：固定广告位 尺寸：300x250-->
                <script type="text/javascript">
                    _acK({aid: 122905, format: 0, mode: 1, gid: 1, serverbaseurl: "afp.csbew.com/"});
                </script>

            </div>
        </div>

    </div>


</div>
<!-- more -->
<script type="text/javascript" src="<?php echo $site ?>/static/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $site ?>/static/jquery.touch.js"></script>
<script type="text/javascript" src="<?php echo $site ?>/static/detail.js"></script>
<script>
    $(document).ready(function () {
        if (window.location.hash) {
            $('.ad_display').removeClass('ad_display');
        } else {
            $('.ad_display').addClass('ad_display');
            window.location.hash = 'showAD';
        }
        $('body').on('swiperight', function (e, touch) {
            // if (touch.xAmount>63) {
            window.history.go(-1);
            // }
        });
    });
</script>
<script>
    var sUserAgent = navigator.userAgent.toLowerCase();
    var IsAndroid = sUserAgent.match(/android/i) == "android";
    var IsIos = sUserAgent.match(/iphone os/i) == "iphone os";
    var _aid = '<?php echo guolv($_GET['aid'])?>';
    var _uid = '<?php echo guolv($_GET['uid'])?>';
    var _logined = '<?php echo $is_logined?>';
    var _ip = '<?php echo getip()?>';
    var _host = document.domain;
    var _width = window.screen.width;
    var _height = window.screen.height;
    var _time = parseInt((new Date).getTime() / 1000);
    var _referer = document.referrer;
    var _useragent = navigator.userAgent;
    var _url = window.location.href;
    var _cookie = cookieRead("read");
    var neirong = $('#neirong');
    var open = $('#open');
    var openfont = $('.openfont');
    var codeSmall = $('.code-small');
    var codeBig = $('.code-big');

    $(function () {
        //点开全文绑定事件
        open.on('tap', function (event) {
            event.preventDefault();
            /* Act on the event */
            if (neirong.hasClass('unopen')) {
                openfont.html('收起全文');
                neirong.removeClass('unopen');
            } else {
                openfont.html('展开全文');
                $('html,body').animate({
                    scrollTop: '0px'
                }, 300);
                neirong.addClass('unopen');
            }
        });

        //二维码绑定点击事件
        codeSmall.on('tap', function (event) {
            event.preventDefault();
            /* Act on the event */
            if (codeBig.hasClass('code-big-left')) {
                codeR();
            } else if (codeBig.hasClass('code-big-right')) {
                codeL();
            }
        });


        $('body').on('swiperight', function (event) {
            codeR();
        });

        function codeL() {
            codeBig.show().stop().removeClass('code-big-right').addClass('code-big-left');
        };
        function codeR() {
            codeBig.stop().removeClass('code-big-left').addClass('code-big-right');
        };


        var pattern = /^http:\/\/mmbiz/;
        var prefix = 'http://img01.store.sogou.com/net/a/04/link?appid=100520031&w=710&url=';
        $("img").each(function () {
            var src = $(this).attr('src');
            if (pattern.test(src)) {
                var newsrc = prefix + src;
                $(this).attr('src', newsrc);
            }
            //$('#js_content').autoIMG();
        });
    })

    function checkCookie() {
        if (window.navigator.cookieEnabled) {
            return true;
        } else {
            return false;
        }
    }


    function count() {
        $.post("<?php echo $site?>/count.php?aid=" + _aid + "&uid=" + _uid + "&ip=" + _ip + "&money=<?php echo $data['money']?>" + "&title=<?php echo $data['title']?>" + "&time=" + _time + "&width=" + _width + "&height=" + _height, function (result) {
            return false;
        });
    }

    //用户id，广告id不为空，文章已被分享，并且为非注册用户 跳转到计费接口
    if (_uid !== '' && _aid !== '' && window.location.hash && !_logined) {
        setTimeout('count()', <?php echo $readtime?>);
    }

</script>


<?php
$mysql->free();
$mysql->close();
?> <br>
</body>
</html>
