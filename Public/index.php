<?php
/**
 * $APP_NAME 统一入口
 */

require_once dirname(__FILE__) . '/init.php';

//装载你的接口
DI()->loader->addDirs('xxkp');



/** ---------------- 响应接口请求 ---------------- **/

if (isset($_REQUEST['service'])) {
    $_REQUEST['service'] = ucwords($_REQUEST['service']);
}

$api = new PhalApi();
$rs = $api->response();
$rs->output();

