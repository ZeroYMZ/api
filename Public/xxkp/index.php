<?php
/**
 * Ytzx 统一入口
 */

require_once dirname(__FILE__) . '/../init.php';

//装载你的接口
DI()->loader->addDirs('Xxkp');

//客户端检查
DI()->filter = 'Common_ClientCheck';

//返回格式规则制定
DI()->response = 'Common_Response';


/** ---------------- 响应接口请求 ---------------- **/

$api = new PhalApi();
$rs = $api->response();
$rs->output();


