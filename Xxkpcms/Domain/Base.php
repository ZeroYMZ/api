<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/30
 * Time: 11:05
 */
class Domain_Base
{

    /**更新用户状态
     * @param $id
     * @param $is_vip
     * @return TRUE
     * @throws PhalApi_Exception_BadRequest
     */
    public function toVip($cuid)
    {

        $modelUser = new Model_User();
        $data = array(
            'is_vip' => 1
        );
        $rs = $modelUser->toVip($cuid, $data);
        if ($rs >= 1) {
            $domainOrder = new Domain_Order();
            $domainOrder->addOrder($cuid, 0);
            return $rs;
        } else {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }
    }

    /**验证签名工具
     * @param $s
     * @return bool
     */
    public static function verfiySign($s)
    {
        $trueS = DI()->config->get('app.xxkpConfig')['s'];
        if ($s !== $trueS) {
            return false;
        } else {
            return true;
        }
    }

    /**验证cookie_id工具
     * @param $cuid
     * @param $cookie_id
     * @return bool
     */
    public static function verifyCookieId($cuid, $cookie_id)
    {
        $model = new Model_User();
        $rs = $model->getCuid($cuid);
        if ($rs) {
            $true_cookie_id = $rs['cookie_id'];
            if ($cookie_id == $true_cookie_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}


