<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 15:03
 */
class Domain_Video
{

    /**添加视频
     * @param $category_id
     * @param $img_url
     * @param $title
     * @param $slogan
     * @param $detail
     * @param $video_url
     * @param $label
     * @return array
     * @throws PhalApi_Exception_InternalServerError
     */

    public function addVideo($category_id, $img_url, $title, $slogan, $detail, $video_url, $label)
    {

        $current_time = date('Y-m-d h:i:s', time());
        $data = array(
            'id' => null,
            'category_id' => $category_id,
            'img_url' => $img_url,
            'title' => $title,
            'slogan' => $slogan,
            'detail' => $detail,
            'video_url' => $video_url,
            'label' => $label,
            'time' => $current_time,
        );
        $model = new Model_Video();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
        } else {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }
        return $rs;
    }

    /**获取视频
     * @param $type_id
     * @param $page
     * @param $num
     * @return array
     * @throws PhalApi_Exception_InternalServerError
     */
    public function getVideo($category_id, $page, $num)
    {
        $rs = array();
        $model = new Model_Video();
        $datas = $model->getVideo($category_id, $page, $num);
        $count = $category_id?DI()->notorm->video->select('*')->where('category_id = ?',$category_id)->count():DI()->notorm->video->count();
        $rs['channelData'] = $datas;
        $rs['count'] = $count;
        if (!$rs) {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }

        return $rs;
    }

}
