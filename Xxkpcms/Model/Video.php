<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 15:03
 */
class Model_Video extends PhalApi_Model_NotORM
{
    public function getVideo($category_id, $page, $number)
    {
        //排序设置默认值
        //返回数据
        if($category_id){
            return $this->getORM()
                ->select('*')
                ->where('category_id = ?', $category_id)
                ->order('id DESC')
                ->limit($page * $number - $number, $number)
                ->fetchRows();
        }else{
            return $this->getORM()
                ->select('*')
                ->order('id DESC')
                ->limit($page * $number - $number, $number)
                ->fetchRows();
        }

    }
    protected function getTableName($uid)
    {
        return 'video';
    }
}
