<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:22
 */

class Model_Ad extends PhalApi_Model_NotORM
{
    /**获取广告
     * @param $type_id
     * @param $page
     * @param $number
     * @param $order
     * @param $by
     * @return mixed
     */
    public function getAd($type, $page, $number)
    {
        //排序设置默认值
        //返回数据
        if($type){
            return $this->getORM()
                ->select('*')
                ->where('type = ?', $type)
                ->order('id DESC')
                ->limit($page * $number - $number, $number)
                ->fetchRows();
        }else{
            return $this->getORM()
                ->select('*')
                ->order('id DESC')
                ->limit($page * $number - $number, $number)
                ->fetchRows();
        }

    }
    protected function getTableName($id)
    {
        return 'ad';
    }
}
