<?php

class Model_User extends PhalApi_Model_NotORM
{
    public function getCuid($cuid){
        return $this->getORM()
            ->select('id , cookie_id')
            ->where('cuid = ?',$cuid)
            ->fetchRow();
    }
    public function toVip($cuid, $data){
        return $this->getORM()
            ->where('cuid = ?',$cuid)
            ->update($data)
            ->fetchRow();
    }
    protected function getTableName($id)
    {
        return 'user';
    }
}
