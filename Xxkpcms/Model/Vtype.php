<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:57
 */
class Model_Vtype extends PhalApi_Model_NotORM
{
    public function getCategory()
    {
        //排序设置默认值
        //返回数据
        return $this->getORM()
            ->select('*')
            ->order('category_id asc')
            ->fetchAll();
    }
    protected function getTableName($id)
    {
        return 'category';
    }
}
