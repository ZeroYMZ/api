<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 12:59
 */

class Model_Order extends PhalApi_Model_NotORM
{
    public function getOrder($page, $number)
    {
        //排序设置默认值
        //返回数据
        return $this->getORM()
            ->select('*')
            ->order('id DESC')
            ->limit($page * $number - $number, $number)
            ->fetchRows();
    }

    public function getOrderByUid($uid, $page, $number)
    {
        //排序设置默认值
        //返回数据
        return $this->getORM()
            ->select('*')
            ->where('uid = ?',$uid)
            ->order('id DESC')
            ->limit($page * $number - $number, $number)
            ->fetchRows();
    }

    protected function getTableName($oid)
    {
        return 'orders';
    }
}
