<?php

class Common_ClientCheck implements PhalApi_Filter
{

    public function check()
    {
        $data = json_decode(urldecode(DI()->des->decrypt(DI()->request->get('key'))), true);
        $cuid = $data['cuid'];
        $cookie_id = $data['cookie_id'];
        if (!$cuid || !$cookie_id) {
            throw new PhalApi_Exception('cuid或cookie_id缺失',2);
        } else {

            if (!Domain_Base::verifyCookieId($cuid, $cookie_id)) {
                throw new PhalApi_Exception('cuid或cookie_id验证失败',2);
            }
        }
    }
}