<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/6/1
 * Time: 9:58
 */
class Api_Admin extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'addVideo' => array(
                'category_id' => array('name' => 'category_id', 'type' => 'int', 'require' => true, 'desc' => '视频类型'),
                'img_url' => array('name' => 'img_url', 'type' => 'string', 'require' => true, 'desc' => '视频默认图片地址'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '视频默认标题'),
                'slogan' => array('name' => 'slogan', 'type' => 'string', 'require' => true, 'desc' => '视频下方标题'),
                'detail' => array('name' => 'detail', 'type' => 'string', 'require' => true, 'desc' => '视频详情'),
                'video_url' => array('name' => 'video_url', 'type' => 'string', 'require' => true, 'desc' => '视频地址'),
                'label' => array('name' => 'label', 'type' => 'string', 'require' => true, 'desc' => '视频标签'),

            ),
            'getVideoByCategoryId' => array(
                'category_id' => array('name' => 'category_id', 'type' => 'int', 'require' => false, 'desc' => '视频类型'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => true, 'desc' => '每页显示数量'),
            ),
            'getOrder' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => false, 'desc' => '订单号'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => true, 'desc' => '每页显示数'),
            ),
            'uploadImg' => array(

                'img' => array('name' => 'img', 'type' => 'file', 'require' => true, 'range' => array('image/jpeg', 'image/jpg', 'image/png'), 'desc' => '视频默认图片'),
            ),
            'uploadVideo' => array(
                'video' => array('name' => 'video', 'type' => 'file', 'require' => true, 'desc' => '上传视频'),
            ),
            'addCategory' => array(
                'category_name' => array('name' => 'category_name', 'type' => 'string', 'require' => true, 'desc' => '视频类型名称'),
                'category_img' => array('name' => 'category_img', 'type' => 'string', 'require' => true, 'desc' => '视频类型图片'),

            ),
            'setAd' => array(
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '广告类型 0为图片 1为应用'),
                'img' => array('name' => 'img', 'type' => 'string', 'require' => true, 'desc' => '图片地址'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '广告名称'),
                'detail' => array('name' => 'detail', 'type' => 'string', 'require' => true, 'desc' => '广告内容'),
                'url' => array('name' => 'url', 'type' => 'string', 'require' => true, 'desc' => '广告链接'),
            ),
            'uploadFile' => array(
                'file' => array('name' => 'file', 'type' => 'file', 'require' => true, 'desc' => '上传文件'),
            ),
        );
    }

    /**
     * 添加视频(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info.status 用户支付状态
     * @return string msg 提示信息
     */
    public function addVideo()
    {

        $domain = new Domain_Video();
        $info = $domain->addVideo($this->category_id, $this->img_url, $this->title, $this->slogan, $this->detail, $this->video_url, $this->label);

        if (empty($info)) {
            $code = 0;
        }

        $code = 1;
        $info['code'] = $code;

        return $info;
    }

    /**
     * 通过视频类型来获取视频列表并分页
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频对象
     * @return string msg 提示信息
     */
    public function getVideoByCategoryId()
    {

        $domain = new Domain_Video();
        $info = $domain->getVideo($this->category_id, $this->page, $this->num);

        if (empty($info)) {
            $code = 0;
        }

        $code = 1;
        $info['code'] = $code;

        return $info;
    }

    /**
     * 获取订单(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 订单详情
     * @return string info.id 订单id
     * @return string info.uid 订单用户id
     * @return string info.info 订单详情
     * @return string info.type 订单类型 0是初始付费 1是点播付费
     * @return string info.time 订单生成时间
     * @return string msg 提示信息
     */
    public function getOrder()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Order();
        $info = $domain->getOrder($this->uid, $this->page, $this->num);

        if (empty($info)) {
            DI()->logger->debug('订单获取失败', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'failed';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }

    /**
     * 上传图片(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示失败
     * @return string info 图片url
     * @return string msg 提示信息
     */
    public function uploadImg()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $saveName = date('Ymdhis', time());
        $uploadName = $_FILES["img"]["name"];
        $saveDir = $_SERVER['DOCUMENT_ROOT'] . "/PhalApi/Public/upload/xxkp/img/";
        $explodeDir = $_SERVER['HTTP_HOST'] . "/PhalApi/Public/upload/xxkp/img/";
        $arr = explode('.', $uploadName);
        $ext = $arr[count($arr) - 1];
        $saveName = $uploadName;
//        $saveName = $saveName . '.' . $ext;
        $_FILES["img"]["name"] = $saveName;
        if (move_uploaded_file($_FILES["img"]["tmp_name"], $saveDir . $saveName)) {
            $rs['info'] = 'http://' . stripslashes($explodeDir . $saveName);
            $rs['msg'] = 'success';
        } else {
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 上传视频(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示失败
     * @return string info 视频url
     * @return string msg 提示信息
     */
    public function uploadVideo()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $saveName = date('Ymdhis', time());
        $uploadName = $_FILES["video"]["name"];
        $saveDir = $_SERVER['DOCUMENT_ROOT'] . "/PhalApi/Public/upload/xxkp/video/";
        $explodeDir = $_SERVER['HTTP_HOST'] . "/PhalApi/Public/upload/xxkp/video/";
        $arr = explode('.', $uploadName);
        $ext = $arr[count($arr) - 1];
        $saveName = $uploadName;
//        $saveName = $saveName . '.' . $ext;
        $_FILES["video"]["name"] = $saveName;
        if (move_uploaded_file($_FILES["video"]["tmp_name"], $saveDir . $saveName)) {
            $rs['info'] = 'http://' . stripslashes($explodeDir . $saveName);
            $rs['msg'] = 'success';
        } else {
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     *添加视频类型（admin）
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频类型对象
     * @return string msg 提示信息
     */
    public function addCategory()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Vtype();
        $info = $domain->addCategory($this->category_name, $this->category_img);

        if (empty($info)) {
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }

    /**
     * 添加一条广告(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info.status 用户支付状态
     * @return string msg 提示信息
     */
    public function setAd()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Ad();
        $info = $domain->setAd($this->type, $this->img, $this->title, $this->detail, $this->url);

        if (empty($info)) {
            DI()->logger->debug('user not found', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'ad not exists';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }
    /**
     * 上传文件(admin)
     * @desc GET
     * @return int code 操作码，0表示成功，1表示失败
     * @return string info 文件url
     * @return string msg 提示信息
     */
    public function uploadFile()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $saveName = date('Ymdhis', time());
        $uploadName = $_FILES["file"]["name"];

        $saveDir = $_SERVER['DOCUMENT_ROOT'] . "/PhalApi/Public/upload/xxkp/file/";
        $explodeDir = $_SERVER['HTTP_HOST'] . "/PhalApi/Public/upload/xxkp/file/";
        $arr = explode('.', $uploadName);
        $ext = $arr[count($arr) - 1];
        $saveName = $uploadName;
        $_FILES["file"]["name"] = $saveName;
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $saveDir . $saveName)) {
            $rs['info'] = 'http://' . stripslashes($explodeDir . $saveName);
            $rs['msg'] = 'success';
        } else {
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }


    //取消登录签名验证服务
    protected function filterCheck()
    {
    }

}