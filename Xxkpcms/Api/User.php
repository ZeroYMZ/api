<?php

class Api_User extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'login' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
                's' => array('name' => 's', 'type' => 'string', 'require' => false, 'desc' => '签名信息'),
                'channel_id' => array('name' => 'channel_id', 'type' => 'string', 'require' => false, 'desc' => '渠道号'),
                'imsi' => array('name' => 'imsi', 'type' => 'string', 'require' => false, 'desc' => '识别sim卡',),
                'imei' => array('name' => 'imei', 'type' => 'string', 'require' => false, 'desc' => '识别手机',),
            ),
        );
    }

    /**
     * 用户登录
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 返回数据
     * @return object info.id 用户id
     * @return object info.is_vip 0表示否 ，1表示是
     * @return string msg 提示信息
     */
    public function login()
    {
        $code = 1;
        $domain = new Domain_User();
        //解析加密字符
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);

        $info = $domain->login($data['channel_id'], $data['imsi'], $data['imei'], $data['cuid'], $data['s']);

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }

    //取消登录签名验证服务
    protected function filterCheck()
    {
    }
}