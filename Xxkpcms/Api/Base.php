<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/30
 * Time: 11:05
 */
class Api_Base extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'toVip' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
            ),
        );
    }

    /**
     * 支付成功后变成vip并生成相应订单
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 数据对象
     * @return string info.id 用户ID
     * @return string info.is_vip 0不是vip 1是vip
     * @return string msg 提示信息
     */
    public function toVip()
    {
        $code = 1;
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);
        $domain = new Domain_Base();
        $info = $domain->toVip($data['cuid']);

        if (empty($info)) {
            $code = 0;
        }
        $info['code'] = $code;

        return $info;
    }
}