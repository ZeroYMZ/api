<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 15:03
 */
class Api_Video extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'getVideoByCategoryId' => array(
                'category_id' => array('name' => 'category_id', 'type' => 'int', 'require' => false, 'desc' => '视频类型'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => false, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => false, 'desc' => '每页显示数量'),

            ),
            'getCategory' => array(
            ),

        );
    }

    /**
     * 通过视频类型来获取视频列表并分页
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频对象
     * @return string msg 提示信息
     */
    public function getVideoByCategoryId()
    {

        $code = 1;
        $domain = new Domain_Video();
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);
        $info = $domain->getVideo($data['category_id'], $data['page'], $data['num']);

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }


    /**
     * 获取所有视频类型
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频类型对象
     * @return string msg 提示信息
     */
    public function getCategory()
    {
        $code = 1;
        $domain = new Domain_Vtype();
        $info = $domain->getCategory();

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }


}