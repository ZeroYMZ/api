<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:22
 */
class Api_Ad extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(

            'getAd' => array(
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '广告类型，0-图片广告，1-app广告'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => true, 'desc' => '显示数量'),
            ),

        );
    }


    /**
     * 通过类型获取广告
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info.status 用户支付状态
     * @return string msg 提示信息
     */
    public function getAd()
    {
        $code = 1;
        $domain = new Domain_Ad();
        $info = $domain->getAd($this->type, $this->page, $this->num);

        if (empty($info)) {
            $code = 0;
        }
        $info['code'] = $code;

        return $info;
    }

}