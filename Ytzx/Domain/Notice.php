<?php

/**用户通知类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 10:29
 */
class Domain_Notice
{
    /**获取通知并分页
     * @param $type 通知类型
     * @param $page 页数
     * @param $number 页量
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function getNotice($type, $page, $number)
    {
        $model = new Model_Notice();
        $rs = $model->getNotice($type, $page, $number);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('新闻已被全部加载 或者数据库访问出问题', 1);
        }
    }
    /**发布通知
     * @param $type 通知类型
     * @param $title 通知标题
     * @param $content 通知内容
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function setNotice($type, $title, $content)
    {
        $currentTime = time();
        $model = new Model_Notice();
        $data = array(
            'id' => null,
            'type' => $type,
            'title' => $title,
            'content' => $content,
            'time' => $currentTime,
        );
        $id = $model->insert($data);
        if ($id) {
            //返回通知id
            return $id;
        } else {
            throw new PhalApi_Exception_BadRequest('发布新闻出现问题，请检查接口参数，没问题请联系管理员', 1);
        }
    }
}