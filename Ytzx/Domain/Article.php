<?php

/**咨询文章类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/15
 * Time: 14:54
 */
class Domain_Article
{
    //文章阅读收益
    protected $article_read_money;
    //文章分享收益
    protected $article_share_money;

    function __construct()
    {
        // 每页显示文章数量
        $this->article_read_money = intval(DI()->config->get('app.baseConfig')['article_read_money']);
        $this->article_share_money = intval(DI()->config->get('app.baseConfig')['article_share_money']);
    }

    /**添加咨询类别
     * @param $name 类别名称
     * @return long 类别id
     * @throws PhalApi_Exception_InternalServerError
     */
    public function addType($name)
    {
        $model = new Model_ArticleType();
        $data = array(
            'id' => null,
            'article_type_name' => $name,
        );
        $typeId = $model->insert($data);
        if ($typeId) {
            return $typeId;
        } else {
            throw new PhalApi_Exception_InternalServerError('添加类别失败', 1);
        }
    }

    /**获取咨询文章类别列表 或者单个类别信息
     * @param $id
     * @return array
     */
    public function getArticleTypesList($id)
    {
        $model = new Model_ArticleType();
        if ($id) {
            $rs = $model->get($id);
        } else {
            $rs = $model->getArticleAllTypes();
        }
        return $rs;
    }

    /**通过类别来获取文章列表并分页
     * @param $type_id
     * @param $page
     * @param $number
     * @param $order
     * @param $by
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */

    public function getArticlesListByType($type_id, $page, $number, $order, $by)
    {
        //$page 文章列表页数
        $type_id = intval($type_id);
        $page = intval($page);
        $number = intval($number);

        $model = new Model_Article();
        $rs = $model->getArticleListByTypeId($type_id, $page, $number, $order, $by);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据不存在', 1);
        }
    }

    /**用户创建一个文章
     * @param $uid
     * @param $title
     * @param $types
     * @param $content
     * @param $thumb
     * @param $belong_type
     * @param $pv_max
     * @param $read_money
     * @param $release_time
     * @param $label
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function createOneArticleByUser($uid, $title, $types, $content, $thumb, $belong_type, $pv_max, $read_money, $release_time, $label)
    {
        $data = array(
            'article_uid' => $uid,
            'article_title' => $title,
            'article_types' => $types,
            'article_is_top' => 0,
            'article_content' => $content,
            'article_thumb' => $thumb,
            'article_belong_type' => $belong_type,
            'article_pv' => 0,
            'article_pv_max' => $pv_max,
            'article_readed_money' => $read_money,
            'article_release_time' => $release_time,
            'article_label' => $label,
        );
        $model = new Model_Article();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入数据失败', 1);
        }
    }

    /**管理员创建一个文章
     * @param $article_types
     * @param $article_is_top
     * @param $article_title
     * @param $article_types
     * @param $article_content
     * @param $article_thumb
     * @param $article_belong_type
     * @param $article_pv_max
     * @param $article_pv
     * @param $article_readed_money
     * @param $article_release_time
     * @param $article_label
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function createOneArticleByAdmin($article_types, $article_is_top, $article_title, $article_types, $article_content, $article_thumb, $article_belong_type, $article_pv_max, $article_pv, $article_readed_money, $article_release_time, $article_label)
    {
        $data = array(
            'uid' => 0,
            'article_title' => $article_title,
            'article_types' => $article_types,
            'article_is_top' => $article_is_top,
            'article_content' => $article_content,
            'article_thumb' => $article_thumb,
            'article_belong_type' => $article_belong_type,
            'article_pv' => $article_pv,
            'article_pv_max' => $article_pv_max,
            'article_readed_money' => $article_readed_money,
            'article_release_time' => $article_release_time,
            'article_label' => $article_label,
        );
        $model = new Model_Article();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入数据失败', 1);
        }
    }

    //删除一篇文章
    public function deleteOneArticle($id)
    {
        $model = new Model_Article();
        $rs = $model->delete($id);
        //日志记录
        DI()->logger->info('delete article', $id);
        if ($rs) {
            return $rs;
        } else {
            throw  new PhalApi_Exception_BadRequest('数据库删除失败', 1);
        }
    }

    //文章有效阅读增加
    public function addPv($id)
    {
        DI()->logger->info('文章阅读书增加', $id);
        $model = new Model_Article();
        //点击量+1
        $data = array(
            'article_pv' => new NotORM_Literal("article_pv + 1")
        );
        $rs = $model->update($id, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }

    }

    //文章加亮
    public function addPvMax($id, $qualities)
    {
        DI()->logger->info('文章增加' . $qualities . '量', $id);
        $model = new Model_Article();
        $pv = $model->get($id, 'article_pv_max');
        $data = array('article_pv_max' => intval($pv['article_pv_max']) + intval($qualities));
        $rs = $model->update($id, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }

    }

    public function editArticleById($id, $article_types, $article_is_top, $article_title, $article_content, $article_thumb, $article_belong_type, $article_readed_money, $article_release_time, $article_label)
    {
        $model = new Model_Article();

        $article = $model->get($id);
        $data = array(
            'article_title' => $article_title ? $article_title : $article['article_title'],
            'article_types' => $article_types ? $article_types : $article['article_types'],
            'article_is_top' => $article_is_top ? $article_is_top : $article['article_is_top'],
            'article_content' => $article_content ? $article_content : $article['article_content'],
            'article_thumb' => $article_thumb ? $article_thumb : $article['article_thumb'],
            'article_belong_type' => $article_belong_type ? $article_belong_type : $article['article_belong_type'],
            'article_readed_money' => $article_readed_money ? $article_readed_money : $article['article_readed_money'],
            'article_release_time' => $article_release_time ? $article_release_time : $article['article_release_time'],
            'article_label' => $article_label ? $article_label : $article['article_label'],
        );

        $rs = $model->update($id, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }
    }

    public function shared($aid, $uid)
    {
        $rs = self::addSharedNum($aid);
        if ($uid) {
            $moneyChange = $this->article_share_money;
            $info = '用户' . $uid . '分享文章' . $aid;
            Domain_Base::insertRefer($uid, 4, $info, $moneyChange);
            $articleDomain = new Domain_User();
            $articleDomain->setUserMoneyCurrent($uid, $moneyChange, 0, 0);
        }
        return $rs;
    }

    public function addSharedNum($aid)
    {
        $model = new Model_Article();
        $sharedNum = array(
            'article_share' => new NotORM_Literal("article_share + 1")
        );
        $result = $model->update($aid, $sharedNum);
        if ($result >= 1) {
            //成功
            return $result;
        } else if ($result === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($result === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }
    }
}