<?php

/**用户提现类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 13:07
 */
class Domain_UserTx
{
    /**发起一个用户提现请求
     * @param $uid 用户id
     * @param $user_sx_1_id 用户一级上线id
     * @param $alipay 支付宝账号
     * @param $alipay_name 支付宝姓名
     * @param $phone 用户手机号
     * @param $money 提现金额
     * @param $time 发起请求时间
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function setUserTx($uid, $user_sx_1_id, $alipay, $alipay_name, $phone, $money, $time)
    {
        $model = new Model_UserTx();
        $data = array(
            'id' => null,
            'uid' => $uid,
            'user_sx_1_id' => $user_sx_1_id ? $user_sx_1_id : '',
            'alipay_name' => $alipay_name,
            'phone' => $phone,
            'money' => $money,
            'status' => 0,
            'time' => $time,
        );

        $orderId = $model->insert($data);
        if ($orderId >= 0) {
            return $this->getUserTxById($orderId);
        } else {
            throw new PhalApi_Exception_BadRequest('订单申请失败', 1);
        }
    }

    /**通过用户uid获取用户相关信息
     * @param $uid 用户id
     * @param $page 页数
     * @param $number 页量
     * @param $status 状态
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function getUserTxByUid($uid, $page, $number, $status)
    {
        $model = new Model_UserTx();
        if (isset($status)) {
            //如果提交了状态
            $rs = $model->getUserTxByUidAndStatus($uid, $page, $number, $status);
        } else {
            $rs = $model->getUserTxByUid($uid, $page, $number);
        }
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('订单不存在', 1);
        }
    }

    /**通过提现号获取用户相关信息
     * @param $id 提现号
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function getUserTxById($id)
    {
        $model = new Model_UserTx();
        $rs = $model->get($id);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('订单不存在', 1);
        }
    }

    /**修改用户提现状态
     * @param $id 提现号
     * @param $status 状态 0待处理 1确认 2拒绝
     * @return TRUE
     * @throws PhalApi_Exception_BadRequest
     */
    public function updateUserTxStatus($id, $status)
    {
        $currentTime = time();
        $model = new Model_UserTx();
        if($status == 1){
            //获取提现用户相关信息
            $info = $this->getUserTxById($id);
            $uid = $info['uid'];
            $money = $info['money'];
            //调用设置金额方法
            $domain = new Domain_User();
            $domain->setUserMoneyCurrent($uid, $money, 0, 1);
        }
        //改变订单状态数据
        $data = array(
            'status' => $status,
            'time' => $currentTime,
    );
        //更新
        $rs = $model->update($id, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }
    }
}