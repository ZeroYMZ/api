<?php
/**权限类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/18
 * Time: 16:17
 */
class Domain_Auth
{
    //登陆送钱
    protected $reward_money_by_login;
    //注册送钱
    protected $reward_money_by_register;
    //初始化
    function __construct()
    {
        $this->reward_money_by_register = DI()->config->get('app.baseConfig')['reward_money_by_register'];
        $this->reward_money_by_login = DI()->config->get('app.baseConfig')['reward_money_by_login'];
    }

    /**登陆
     * @param $user_phone
     * @param $user_password
     * @param $user_device_name
     * @param $user_device_size
     * @param $user_logined_ip
     * @return array|mixed
     * @throws PhalApi_Exception_BadRequest
     */


    public function loginIn($user_phone, $user_password, $user_device_sys, $user_device_name, $user_device_size, $user_logined_ip)
    {
        $rs = array();
        //数据格式处理
        $user_phone = strval($user_phone);
        $user_password = md5($user_password);
        //实例化需要的model
        $userModel = new Model_UserInfo();
        $referModel = new Model_RefersList();
        //通过手机获取用户密码
        $rs = $userModel->getUserByPhone($user_phone);
        $uid = $rs['id'];
        //如果密码相等
        if ($this->checkLoginAuth($user_phone, $user_password)) {
            $currentTime = time();
            //插入登陆统计
            $loginInfo = '用户' . $uid . ': 登陆';
            $money = 0;
            $referId = Domain_Base::insertRefer($uid, 0, $loginInfo, $money);
            //更新用户最新登陆时间
            $data = array('user_last_logined_time' => $currentTime);
            $userModel->update($uid, $data);
            //记录设备
            Domain_Base::recordDevice($uid, $user_logined_ip, $user_device_sys, $user_device_name, $user_device_size, $currentTime);
            //看登陆时间是否超过一天 超过一天登陆送金额 不超过就不送
            if ($currentTime - 60 * 60 * 24 > $rs['user_login_accept_reward_time']) {
                $change = array('user_login_accept_reward_time' => $currentTime);
                $userModel->update($uid, $change);
                $refer = array(
                    'refer_type' => 0,
                    'refer_money' => $this->reward_money_by_login,
                    'refer_title' => '用户' . $uid . '每日签到登陆赠送',
                );
                $referModel->update($referId, $refer);
                $domain = new Domain_User();
                $rs = $domain->setUserMoneyCurrent($uid, $this->reward_money_by_login ? $this->reward_money_by_login : 0, 0, 0);
            } else {
                $rs = $userModel->get($uid);
            }
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('密码不正确', 1);
        }
    }

    /**检查是否可以登陆
     * @param $user_phone
     * @param $user_password
     * @return bool
     */
    public function checkLoginAuth($user_phone, $user_password)
    {
        $rs = array();
        $model = new Model_UserInfo();
        $rs = $model->getUserByPhone($user_phone);
        if ($rs['user_password'] == $user_password) {
            return true;
        } else {
            return false;
        }
    }

    /**注册
     * @param $vcode
     * @param $user_phone
     * @param $user_password
     * @param $user_device_name
     * @param $user_device_size
     * @param $user_register_ip
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function register($vcode, $user_phone, $user_password, $user_device_sys, $user_device_name, $user_device_size, $user_register_ip)
    {
        $rs = array();

        $user_password = md5($user_password);
        $vcode = intval($vcode);
        $model = new Model_UserInfo();

        //发送短信
        $result = DI()->msg->send($user_phone);

        if ($result) {
            //验证注册权限
            if ($this->checkRegisterAuth($vcode, $user_phone, $user_password)) {
                $currentTime = time();
                //注册成功送钱
                $data = array(
                    'user_phone' => $user_phone,
                    'user_password' => $user_password,
                    'user_register_time' => $currentTime,
                    'user_money_current' => $this->reward_money_by_register ? $this->reward_money_by_register : 0,
                    'user_money_all' => $this->reward_money_by_register ? $this->reward_money_by_register : 0,
                    'user_last_logined_time' => $currentTime,
                    'user_login_accept_reward_time' => $currentTime,//更新登陆签到时间
                );
                $uid = $model->insert($data);
                //插入注册统计
                $info = '新用户' . $uid . ': 注册并登录';
                $money = $this->reward_money_by_register + $this->reward_money_by_login;
                Domain_Base::insertRefer($uid, 1, $info, $money);
                //记录设备
                Domain_Base::recordDevice($uid, $user_device_sys, $user_register_ip, $user_device_name, $user_device_size, $currentTime);
                //登陆送钱
                $domain = new Domain_User;
                $rs = $domain->setUserMoneyCurrent($uid, $this->reward_money_by_login ? $this->reward_money_by_login : 0, 0, 0);
                return $rs;
            } else {
                throw new PhalApi_Exception_BadRequest('电话已被注册', 1);
            }
        } else {
            throw new PhalApi_Exception_BadRequest('短信平台发生错误', 2);
        }
    }


    /**检测注册权限
     * @param $vcode 验证码
     * @param $user_phone 用户电话
     * @param $user_password 密码
     * @return bool
     * @throws PhalApi_Exception_BadRequest
     */
    public function checkRegisterAuth($vcode, $user_phone, $user_password)
    {
        $rs = array();
        if ($this->verifyVcode($vcode)) {
            $model = new Model_UserInfo();
            $rs = $model->getUserByPhone($user_phone);
            if ($rs) {
                return false;
            } else {
                return true;
            }
        } else {
            throw new PhalApi_Exception_BadRequest('验证码错误', 1);
        }
    }

    /**用户修改密码
     * @param $vcode
     * @param $user_phone
     * @param $new_password
     * @return array|mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function changeUserPassWord($vcode, $user_phone, $new_password)
    {
        $rs = array();
        if ($this->verifyVcode($vcode)) {
            $user_phone = strval($user_phone);
            $new_password = md5($new_password);
            $model = new Model_UserInfo();

            $model->changeUserPassWord($user_phone, $new_password);
            $rs = $model->getUserByPhone($user_phone);
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('验证码错误', 1);
        }
    }

    /**短信验证码验证
     * @param $v_code int 验证码
     * @return bool
     */
    public function verifyVcode($v_code)
    {
        $result = DI()->msg->verify($v_code);
        return $result;
    }

    /**发送短信验证码
     * @param $v_code
     * @return bool
     */
    public function sendVcode($user_phone)
    {
        $result = DI()->msg->send($user_phone);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

}

