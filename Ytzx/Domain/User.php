<?php

/**资讯用户类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/18
 * Time: 16:17
 */
class Domain_User
{

    /**通过id获取用户信息
     * @param $uid
     * @return array|mixed
     */

    public function getUserInfo($uid)
    {
        $rs = array();
        $uid = intval($uid);
        $model = new Model_UserInfo();
        $rs = $model->get($uid);
        if($rs){
            return $rs;
        }elseif($rs == ''){
            throw new PhalApi_Exception_BadRequest('用户不存在',1);
        }else{
            throw new PhalApi_Exception_BadRequest('数据库内部出错',2);

        }
    }

    /**通过id获取用户信息
     * @param $uid
     * @param $page
     * @param $number
     * @return array|mixed
     * @throws PhalApi_Exception_InternalServerError
     */

    public function getUserFriend($uid, $page, $number)
    {
        $rs = array();
        $uid = intval($uid);
        $model = new Model_UserInfo();
        $rs = $model->getUserFriend($uid, $page, $number);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('用户好友列表为空', 1);
        }
    }

    /**设置用户推荐人id
     * @param $uid 用户id
     * @param $tjid 推荐人id
     * @return array|mixed
     */
    public function setUserTjId($uid, $tjid)
    {
        $rs = array();

        $uid = intval($uid);
        $tjid = intval($tjid);

        $data = array('tj_id' => $tjid);
        $model = new Model_UserInfo();
        $rs = $model->update($uid, $data);

        return $rs;
    }

    /**更改用户账户金额（当前金额，总金额，好友累计金额）
     * @param $uid 用户id
     * @param $change_money 改变金额
     * @param $change_type 改变类型 0当前余额 1好友累计
     * @param $change_way 改变方式 0加法 1减法
     * @return array|mixed 返回用户最新信息
     */
    public function setUserMoneyCurrent($uid, $change_money, $change_type, $change_way)
    {
        //获取分成率
        $rs = array();
        $model = new Model_UserInfo();
        if ($change_type == 0) {
            //用户当前余额更改
            $rows = $model->get($uid, 'tj_id, user_money_all, user_money_current');
            if ($change_way == 0) {
                //增加用户金额 （收益）
                $moneyAllChange = floatval($rows['user_money_all']) + floatval($change_money);
                $moneyCurrentChange = floatval($rows['user_money_current']) + floatval($change_money);
                $data = array(
                    'user_money_all' => $moneyAllChange,
                    'user_money_current' => $moneyCurrentChange,
                );
                $result = $model->update($uid, $data);
                if ($result) {//如果用户增加金额成功
                    //判断用户是否有师傅
                    if (intval($rows['tj_id']) > 0) {
                        //如果有师傅
                        $id = intval($rows['tj_id']);
                        //调用上线分成方法
                        self::sxfc($id, $uid, $change_money);
                    }
                } else {

                }
            } elseif ($change_way == 1) {
                //减少用户金额 （提现）
                $moneyCurrentChange = floatval($rows['user_money_current']) - floatval($change_money);
                $data = array('user_money_current' => $moneyCurrentChange);
                $model->update($uid, $data);
            }
            $rs = $model->get($uid);

        } elseif ($change_type == 1) {
            //用户友情好友累计更改
            $rows = $model->get($uid, 'user_money_all, user_money_by_friend');
            if ($change_way == 0) {
                //增加用户金额 （收益）
                $moneyAllChange = floatval($rows['user_money_all']) + floatval($change_money);
                $moneyFriendChange = floatval($rows['user_money_by_friend']) + floatval($change_money);
                $data = array(
                    'user_money_all' => $moneyAllChange,
                    'user_money_by_friend' => $moneyFriendChange,
                );
                $model->update($uid, $data);
                $rs = $model->get($uid);
            } elseif ($change_way == 1) {
                //减少用户金额 （提现）
                $moneyFriendChange = floatval($rows['user_money_by_friend']) - floatval($change_money);
                $data = array('user_money_by_friend' => $moneyFriendChange);
                $model->update($uid, $data);
                $rs = $model->get($uid);
            }
        }
        return $rs;
    }

    /**上线分成
     * @param $id 上线id
     * @param $uid 下线id
     * @param $change_money 变动金额
     * @throws PhalApi_Exception
     * @throws PhalApi_Exception_BadRequest
     */
    protected function sxfc($id, $uid, $change_money)
    {
        $rate = DI()->config->get('app.baseConfig')['rate'];
        $model = new Model_UserInfo();
        $rows = $model->get($id, 'user_money_all, user_money_current, user_money_by_friend');
        //有师傅 给提成
        $tc = floatval($change_money * $rate / 100);
        $tc_data = array(
            'user_money_all' => floatval($rows['user_money_all']) + $tc,
            'user_money_current' => floatval($rows['user_money_current']) + $tc,
            'user_money_by_friend' => floatval($rows['user_money_by_friend']) + $tc,
        );
        $result = $model->update($id, $tc_data);
        if ($result) {
            //成功 插入提成信息
            $info = '用户' . $id . '获取好友' . $uid . '分成';
            Domain_Base::insertRefer($id, 2, $info, $tc);
        } else {
            throw new PhalApi_Exception_BadRequest('师傅分成出现异常', 1);
        }
    }

    /**设置用户支付宝信息
     * @param $uid
     * @param $alipay
     * @param $alipay_name
     * @return TRUE
     * @throws PhalApi_Exception_BadRequest
     */
    public function setUserPayInfo($uid, $alipay, $alipay_name)
    {
        $model = new Model_UserInfo();
        $data = array(
            'id' => $uid,
            'user_alipay' => $alipay,
            'user_alipay_name' => $alipay_name,
        );
        $rs = $model->update($uid, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }
    }

    /**设置用户信息
     * @param $id
     * @param $tjId
     * @param $phone
     * @param $alipay
     * @param $alipay_name
     * @return TRUE
     * @throws PhalApi_Exception_BadRequest
     */
    public function editUserInfo($id, $tjId, $phone, $alipay, $alipay_name)
    {
        $model = new Model_UserInfo();
        $userInfo = $model->get($id);

        $data = array(
            'tj_id' => $tjId ? $tjId : $userInfo['tj_id'],
            'user_phone' => $phone ? $phone : $userInfo['user_phone'],
            'user_alipay' => $alipay ? $alipay : $userInfo['user_alipay'],
            'user_alipay_name' => $alipay_name ? $alipay_name : $userInfo['user_alipay_name'],
        );
        $rs = $model->update($id, $data);
        if ($rs >= 1) {
            //成功
            return $rs;
        } else if ($rs === 0) {
            //相同数据，无更新
            throw  new PhalApi_Exception_BadRequest('相同数据，无更新', 1);
        } else if ($rs === false) {
            //更新失败
            throw  new PhalApi_Exception_BadRequest('更新失败', 2);
        }
    }


}


