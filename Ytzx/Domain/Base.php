<?php

/**基础类 公用domain方法
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/20
 * Time: 16:15
 */
class Domain_Base
{
    /**设备记录
     * @param $uid
     * @param $user_device_sys
     * @param $user_device_name
     * @param $user_device_size
     * @param $time
     */
    public static function recordDevice($uid, $user_device_sys, $user_login_ip, $user_device_name, $user_device_size, $time)
    {
        $data = array(
            'id' => $uid,
            'user_device_sys' => $user_device_sys ? $user_device_sys : 0,
            'user_login_ip' => $user_login_ip ? $user_login_ip : 0,
            'user_device_name' => $user_device_name ? $user_device_name : 0,
            'user_device_size' => $user_device_size ? $user_device_size : 0,
            'user_last_logined_time' => $time,
        );
        $model = new Model_UserDevice();
        $model->insert($data);
    }

    /**插入统计
     * @param $data
     * @return bool
     * @throws PhalApi_Exception_BadRequest
     */
    public static function insertRefer($uid, $type, $info, $money)
    {
        $currentTime = time();
        $refer = array(
            'id' => null,
            'refer_type' => $type,
            'uid' => $uid,
            'aid' => -1,//文章id -1时表示不是文章收入
            'refer_title' => $info,
            'refer_money' => $money,
            'refer_time' => $currentTime,
        );
        $model = new Model_RefersList();
        $referId = $model->insert($refer);
        if ($referId > 0) {
            return $referId;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入refer错误', 1);
        }

    }
}