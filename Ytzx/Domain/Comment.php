<?php
/**文章评论业务处理类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/22
 * Time: 10:28
 */
class Domain_Comment
{
    /**获取评论列表并分页
     * @param $aid 文章id
     * @param $page 页数
     * @param $number 页量
     * @param $order 排序字段
     * @param $by 排序方式
     * @return mixed
     * @throws PhalApi_Exception
     */
    public function getComment($aid, $page, $number, $order, $by)
    {
        $model = new Model_Comment();
        $rs = $model->getComment($aid, $page, $number, $order, $by);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('获取评论已被全部加载 或者数据库访问出问题', 1);
        }
    }
    /**设置用户评论
     * @param $aid 文章id
     * @param $uid 用户id
     * @param $content 文章内容
     * @param $vote 投票数
     * @return mixed
     * @throws PhalApi_Exception
     */
    public function setComment($aid, $uid, $content, $vote)
    {
        $currentTime = time();
        //实例化所需要的model类
        $commentModel = new Model_Comment();
        $articleModel = new Model_Article();
        //评论数据
        $data = array(
            'id' => null,
            'aid' => $aid,
            'uid' => $uid,
            'content' => $content,
            'vote' => $vote ? $vote:0,
            'time' => $currentTime
        );
        $commentId = $commentModel->insert($data);
        //评论数统计
        $commentNum = array(
            'article_comment' => new NotORM_Literal("article_comment + 1")
        );
        $result = $articleModel->update($aid, $commentNum);
        //插入评论统计
        $info = '用户'.$uid.'评论文章'.$aid;
        Domain_Base::insertRefer($uid,5,$info,0);

        if ($commentId && $result) {
            //返回通知id
            return $commentId;
        } else {
            throw new PhalApi_Exception_BadRequest('用户评论出现问题，请检查接口参数，没问题请联系管理员', 1);
        }
    }
    /**点赞
     * @param $id 评论id
     * @return mixed
     * @throws PhalApi_Exception
     */
    public function vote($id)
    {
        $currentTime = time();
        $model = new Model_Comment();
        $data = array(
            'vote' => new NotORM_Literal("vote + 1")
        );
        $commentId = $model->update($id, $data);
        if ($commentId) {
            //返回通知id
            return $commentId;
        } else {
            throw new PhalApi_Exception_BadRequest('点赞出现问题，请检查接口参数，没问题请联系管理员', 1);
        }
    }
    /**管理员删除用户留言
     * @param  $id 评论id
     * @return mixed
     * @throws PhalApi_Exception
     */
    public function deleteComment($id)
    {
        $model = new Model_Comment();
        $result = $model->delete($id);
        if ($result) {
            //返回通知id
            return $result;
        } else {
            throw new PhalApi_Exception_BadRequest('删除留言出现问题，请检查接口参数，没问题请联系管理员', 1);
        }
    }
}