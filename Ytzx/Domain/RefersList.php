<?php

/**统计类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/20
 * Time: 10:09
 */
class Domain_RefersList
{

    public function getRefer($uid, $type, $page, $number)
    {
        $model = new Model_RefersList();
        $rs = $model->getRefer($uid, $type, $page, $number);
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('无统计消息', 1);
        }
    }

}