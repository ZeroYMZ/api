<?php
/**文章评论api
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 10:28
 */
class Api_Comment extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules(){
        return array(
            'getComment' => array(
                'aid' => array('name' => 'aid', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '第几页'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示的数量'),
                'order' => array('name' => 'order', 'type' => 'string', 'require' => false, 'desc' => '排序字段'),
                'by' => array('name' => 'by', 'type' => 'string', 'require' => false, 'desc' => '排序方式'),
            ),
            'setComment' => array(
                'aid' => array('name' => 'aid', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '评论内容'),
                'vote' => array('name' => 'vote', 'type' => 'int', 'require' => false, 'desc' => '评论投票数 如果不设置则默认为0'),
            ),
            'vote' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '评论id'),
            ),
            'deleteComment' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '评论id'),
            ),
        );
    }

    /**
     * 通过文章id获取文章评论
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function getComment() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Comment();
        $info = $domain->getComment($this->aid, $this->page, $this->number, $this->order, $this->by);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     *用户发起评论
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function setComment() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Comment();
        $info = $domain->setComment($this->aid, $this->uid, $this->content, $this->vote);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     *用户点赞
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function vote() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Comment();
        $info = $domain->vote($this->id);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     *删除用户留言 （admin）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function deleteComment() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Comment();
        $info = $domain->deleteComment($this->id);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
}