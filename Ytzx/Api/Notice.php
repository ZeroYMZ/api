<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 10:28
 */
class Api_Notice extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules(){
        return array(
            'getNotice' => array(
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '通知类型 0是新手帮助 1是用户消息通知'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '第几页'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示的数量'),
            ),
            'setNotice' => array(
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '通知类型 0是新手帮助 1是用户消息通知'),
                'title' => array('name' => 'type', 'type' => 'string', 'require' => true, 'desc' => '通知标题'),
                'content' => array('name' => 'type', 'type' => 'string', 'require' => true, 'desc' => '通知内容'),
            ),
        );
    }

    /**
     * 获取用户通知
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function getNotice() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Notice();
        $info = $domain->getNotice($this->type, $this->page, $this->number);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     *设置用户通知（admin）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function setNotice() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Notice();
        $info = $domain->setNotice($this->type, $this->title, $this->content);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
}