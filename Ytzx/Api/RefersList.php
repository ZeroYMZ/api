<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/20
 * Time: 9:46
 */
class Api_RefersList extends PhalApi_Api {
    /*定制请求参数规定*/
    public function getRules(){
        return array(
            'getRefer' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id'),
                'type' => array('name' => 'type', 'type' => 'int', 'require' => false, 'desc' => '统计类型'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '第几页'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示数量'),
            ),
        );
    }

    /**
     * 按不同类型获取统计
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function getRefer() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_RefersList();
        $info = $domain->getRefer($this->uid, $this->type, $this->page, $this->number);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
}