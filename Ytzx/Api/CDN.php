<?php
/*
 * +----------------------------------------------------------------------
 * | 上传接口
 * +----------------------------------------------------------------------
 * | Copyright (c) 2015 summer All rights reserved.
 * +----------------------------------------------------------------------
 * | Author: summer <aer_c@qq.com> <qq7579476>
 * +----------------------------------------------------------------------
 * | This is not a free software, unauthorized no use and dissemination.
 * +----------------------------------------------------------------------
 * | Date
 * +----------------------------------------------------------------------
 */


class Api_CDN extends PhalApi_Api {

    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'uploadFile' => array(
                'file' => array('name' => 'file', 'type' => 'file', 'require' => true, 'desc' => '文件'),
            ),
        );
    }
    /**
     * 图片上传接口
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return string url 绝对路径
     * @return string file 相对路径，用于保存至数据库
     * @return string msg 提示信息
     */
    public function uploadFile() {
        $rs = array('code' => 1, 'url' => '', 'msg' => T('fail to upload file'));

        //设置上传路径 设置方法参考3.2
        $pathDate = date('Y/m/d',time());
        DI()->ucloud->set('Ytzx',$pathDate);

        //上传表单名
        $res = DI()->ucloud->upfile($_FILES['file']);

        if ($res) {
            $rs['code'] = 0;//成功
            $rs['url'] = $res['url']; //绝对路径
            $rs['file'] = $res['file']; //相对路径，用于保存至数据库，按项目情况自己决定吧
            $rs['msg'] = '上传成功';
        }
        return $rs;
    }
}
