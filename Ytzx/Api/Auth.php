<?php

class Api_Auth extends PhalApi_Api
{   
	/*定制请求参数规定*/
    public function getRules(){
    	return array(
    		'register' => array(
    			'vcode' => array('name' => 'vcode', 'type' => 'int', 'require' => true, 'desc' => '验证码', 'min' => 4),
    			'user_phone' => array('name' => 'user_phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号', 'min' => 11, 'max' => 11),
    			'user_password' => array('name' => 'user_password', 'type' => 'string', 'require' => true, 'desc' => '用户密码', 'min' => 4),
                'user_register_ip' => array('name' => 'user_register_ip', 'type' => 'string', 'require' => false, 'desc' => '用户注册ip'),
                'user_device_sys' => array('name' => 'user_device_sys', 'type' => 'string', 'require' => false, 'desc' => '用户设备系统'),
                'user_device_name' => array('name' => 'user_device_name', 'type' => 'string', 'require' => false, 'desc' => '用户设备名称'),
                'user_device_size' => array('name' => 'user_device_size', 'type' => 'string', 'require' => false, 'desc' => '用户设备尺寸'),
            ),
    		'loginIn' => array(
    			'user_phone' => array('name' => 'user_phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号', 'min' => 11, 'max' => 11),
                'user_password' => array('name' => 'user_password', 'type' => 'string', 'require' => true, 'desc' => '用户密码', 'min' => 4),
                'user_device_sys' => array('name' => 'user_device_sys', 'type' => 'string', 'require' => false, 'desc' => '用户设备系统'),
    			'user_device_name' => array('name' => 'user_device_name', 'type' => 'string', 'require' => false, 'desc' => '用户设备名称'),
    			'user_device_size' => array('name' => 'user_device_size', 'type' => 'string', 'require' => false, 'desc' => '用户设备尺寸'),
    			'user_login_ip' => array('name' => 'user_logined_ip', 'type' => 'string', 'require' => false, 'desc' => '用户登陆ip'),
            ),
            'changeUserPassWord' => array(
                'vcode' => array('name' => 'vcode', 'type' => 'int', 'require' => true, 'desc' => '验证码', 'min' => 4),
                'user_phone' => array('name' => 'user_phone', 'type' => 'string', 'require' => true, 'min' => 11, 'max' => 11, 'desc' =>'用户登陆手机号'),
                'new_password' => array('name' => 'new_password', 'type' => 'string', 'require' => true, 'desc' => '新密码', 'min' => 4),
            ),
            'sendVcode' => array(
                'user_phone' => array('name' => 'user_phone', 'type' => 'string', 'require' => true, 'min' => 11, 'max' => 11, 'desc' =>'用户登陆手机号'),
            ),
        );
    }

    /**
     * 用户登陆
     * @desc POST
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.user_phone 用户登陆手机号
     * @return string info.user_password 用户登陆密码(加密)
     * @return string msg 提示信息
     */
    public function loginIn() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Auth();
        $info = $domain->loginIn($this->user_phone, $this->user_password, $this->user_device_sys,
            $this->user_device_name, $this->user_device_size, $this->user_login_ip);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }

        return $rs;
    }
    /**
     * 用户注册
     * @desc POST
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id vcode
     * @return string info.user_phone 用户登陆手机号
     * @return string info.user_password 用户登陆密(加密)
     * @return string msg 提示信息
     */
    public function register() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Auth();
        $info = $domain->register($this->vcode, $this->user_phone, $this->user_password, $this->user_device_sys, $this->user_device_name, $this->user_device_size, $this->user_register_ip);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 用户忘记密码功能
     * @desc POST
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.user_phone 用户登陆手机号
     * @return string info.user_password 用户登陆密(加密)
     * @return string msg 提示信息
     */
    public function changeUserPassWord(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Auth();
        $info = $domain -> changeUserPassWord($this->vcode, $this->user_phone,$this->new_password);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 发送短信验证码
     * @desc GET
     * @return int code 操作码，0表示成功，1失败
     * @return string msg 提示信息
     */

    public function sendVcode(){
        $rs = array('code' => 0, 'msg' => '');
        $domain = new Domain_Auth();
        $result = $domain->sendVcode($this->user_phone);
        if($result){
            $rs['code'] = 0;
            $rs['msg'] = '成功';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = '失败';
        }
        return $rs;
    }
    //取消签名认证
    protected function filterCheck(){}
    // protected function Check(){}
}