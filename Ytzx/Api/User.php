<?php

class Api_User extends PhalApi_Api
{   
	/*定制请求参数规定*/
    public function getRules(){
    	return array(
    		'getUserInfo' => array(
    			'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
                ),
            'getUserFriend' => array(
    			'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
    			'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '页数',),
    			'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示数量',),
                ),
    		'setUserTjId' => array(
    			'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
    			'tjid' => array('name' => 'tjid', 'type' => 'int', 'require' => true, 'desc' => '推荐人id', 'min' => 1),
                ),
            'setUserMoneyCurrent' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
                'change_money' => array('name' => 'change_money', 'type' => 'float', 'require' => true, 'desc' => '金额改变'),
                'change_type' => array('name' => 'change_type', 'type' => 'int', 'require' => true, 'desc' => '改变类型，0是当前账户余额账户总额改变，1是好友邀请金额改变'),
                'change_way' => array('name' => 'change_way', 'type' => 'int', 'require' => true, 'desc' => '改变方法，0增加，1减少'),
                ),
            'setUserPayInfo' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
                'user_alipay' => array('name' => 'user_alipay', 'type' => 'string', 'require' => true, 'desc' => '用户支付宝账户'),
                'user_alipay_name' => array('name' => 'user_alipay_name', 'type' => 'string', 'require' => true, 'desc' => '用户支付宝姓名'),
                ),
            'editUserInfo' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '用户id', 'min' => 1),
                'tj_id' => array('name' => 'uid', 'type' => 'int', 'require' => false, 'desc' => '用户推荐人id', 'min' => 1),
                'user_phone' => array('name' => 'user_phone', 'type' => 'string', 'require' => false, 'desc' => '用户手机号', 'min' => 11, 'max' => 11),
                'user_alipay' => array('name' => 'user_alipay', 'type' => 'string', 'require' => false, 'desc' => '用户支付宝账户'),
                'user_alipay_name' => array('name' => 'user_alipay_name', 'type' => 'string', 'require' => false, 'desc' => '用户支付宝姓名'),
                ),
    		);
    }
    /**
     * 获取用户基本信息
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 用户信息对象
     * @return string info.id 用户ID
     * @return string info.tjid 用户推荐人ID
     * @return string info.realname 用户支付宝姓名
     * @return string info.alipay 用户支付宝账号
     * @return string info.ip 用户登陆ip
     * @return string info.phone 用户登陆手机号
     * @return string info.password 用户登陆密(加密)
     * @return string info.money 用户当前金额
     * @return string info.money_all 用户总金额累计
     * @return string info.money_xia 用户好友分红累计
     * @return string msg 提示信息
     */
    public function getUserInfo() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_User();
        $info = $domain->getUserInfo($this->uid);

        if (empty($info)) {
            DI()->logger->debug('user not found', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'user not exists';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }
    /**
     * 获取好友列表信息
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 用户信息对象
     * @return string info.id 用户ID
     * @return string info.tjid 用户推荐人ID
     * @return string info.realname 用户支付宝姓名
     * @return string info.alipay 用户支付宝账号
     * @return string info.ip 用户登陆ip
     * @return string info.phone 用户登陆手机号
     * @return string info.password 用户登陆密(加密)
     * @return string info.money 用户当前金额
     * @return string info.money_all 用户总金额累计
     * @return string info.money_xia 用户好友分红累计
     * @return string msg 提示信息
     */
    public function getUserFriend() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_User();
        $info = $domain->getUserFriend($this->uid, $this->page, $this->number);

        if (empty($info)) {
            DI()->logger->debug('user not found', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'user not exists';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }
    /**
     * 设置用户推荐人id
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string msg 提示信息
     */
    public function setUserTjId() {
        $rs = array('code' => 0, 'msg' => '');

    	$domain = new Domain_User();
        $info = $domain->setUserTjId($this->uid, $this->tjid);

        if (empty($info)) {
            DI()->logger->debug('failed to set tjid', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'user not exists';
        }
        $rs['msg'] = 'success';
        return $rs;
    }
    /**
     * 设置用户当前账户余额（admin app都不可调用 为紧急处理时由后端人员调用）
     * @desc POST
     * @return int code 操作码，0表示成功，1表示失败
     * @return string msg 提示信息
     */
    public function setUserMoneyCurrent() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_User();
        $info = $domain->setUserMoneyCurrent($this->uid, $this->change_money, $this->change_type, $this->change_way);

        if (empty($info)) {
            DI()->logger->debug('failed to set current money', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'failed to set current money';
        }
        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }
    /**
     * 设置用户当前支付宝信息
     * @desc POST
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
    public function setUserPayInfo(){
        $rs = array('code' => 0, 'msg' => '');

        $domain = new Domain_User();
        $info = $domain->setUserPayInfo($this->uid, $this->user_alipay, $this->user_alipay_name);

        if (empty($info)) {
            DI()->logger->debug('设置用户支付宝信息失败', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = '失败';
        }
        $rs['msg'] = '成功';
        return $rs;
    }
    /**
     * 管理员更改用户基本信息（admin）
     * @desc POST
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return object info 用户信息对象
     * @return string info.id 用户ID
     * @return string info.tjid 用户推荐人ID
     * @return string info.realname 用户支付宝姓名
     * @return string info.alipay 用户支付宝账号
     * @return string info.ip 用户登陆ip
     * @return string info.phone 用户登陆手机号
     * @return string info.password 用户登陆密(加密)
     * @return string info.money 用户当前金额
     * @return string info.money_all 用户总金额累计
     * @return string info.money_xia 用户好友分红累计
     * @return string msg 提示信息
     */
    public function editUserInfo() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_User();
        $info = $domain->editUserInfo($this->id, $this->tj_id, $this->user_phone, $this->user_alipay, $this->user_alipay_name);

        if (empty($info)) {
            DI()->logger->debug('user not found', $this->uid);

            $rs['code'] = 1;
            $rs['msg'] = 'user not exists';
        }

        $rs['info'] = $info;
        $rs['msg'] = 'success';

        return $rs;
    }
}