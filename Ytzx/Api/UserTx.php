<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 13:07
 */
class Api_UserTx extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'setUserTx' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '提现用户id'),
                'user_sx_1_id' => array('name' => 'user_sx_1_id', 'type' => 'int', 'require' => false, 'desc' => '提现一级上线id'),
                'alipay' => array('name' => 'alipay', 'type' => 'string', 'require' => true, 'desc' => '支付宝账号'),
                'alipay_name' => array('name' => 'alipay_name', 'type' => 'string', 'require' => true, 'desc' => '支付宝账户名'),
                'phone' => array('name' => 'phone', 'type' => 'string', 'require' => true, 'desc' => '用户手机号'),
                'money' => array('name' => 'money', 'type' => 'float', 'require' => true, 'desc' => '提现金额'),
                'time' => array('name' => 'time', 'type' => 'string', 'require' => true, 'desc' => '提现时间'),
            ),
            'getUserTxByUid' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '提现用户id'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '第几页'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示数'),
                'status' => array('name' => 'status', 'type' => 'int', 'require' => false, 'desc' => '提现状态'),
            ),
            'updateUserTxStatus' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '提现订单号id'),
                'status' => array('name' => 'status', 'type' => 'int', 'require' => true, 'desc' => '订单状态'),
            ),
        );
    }

    /**
     * 用户发起一个提现请求
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function setUserTx()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_UserTx();
        $info = $domain->setUserTx($this->uid, $this->user_sx_1_id, $this->alipay, $this->alipay_name, $this->phone, $this->money, $this->time);
        if (!empty($info)) {
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        } else {
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 通过用户id来获取用户提现信息
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function getUserTxByUid()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_UserTx();
        $info = $domain->getUserTxByUid($this->uid, $this->page, $this->number, $this->status);
        if (!empty($info)) {
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        } else {
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 更改用户提现状态（admin）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function updateUserTxStatus()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_UserTx();
        $info = $domain->updateUserTxStatus($this->id, $this->status);
        if (!empty($info)) {
            $rs['msg'] = 'success';
        } else {
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
}