<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/15
 * Time: 14:54
 */

class Api_Article extends  PhalApi_Api{

    public function getRules()
    {
        return array(
            'addType' => array(
              'name' => array('name' => 'name', 'type' => 'string', 'require' => true, 'desc' => '文章类别名称'),
            ),
            'getArticleTypesList' => array(
              'id' => array('name' => 'id', 'type' => 'int', 'require' => false, 'desc' => '文章类别id'),
          ),
            'getArticlesListByType'=> array(
                'type_id' => array('name' => 'type_id', 'type' => 'int', 'require' => true, 'desc' => '文章类别id'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => true, 'desc' => '文章列表页数'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '每页显示的文章个数'),
                'order' => array('name' => 'order', 'type' => 'string', 'require' => false, 'desc' => '排序的类别'),
                'by' => array('name' => 'by', 'type' => 'string', 'require' => false, 'desc' => '升序还是降序'),
            ),
           'createOneArticleByUser'=> array(
               'uid'=> array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '创建用户id'),
               'type'=> array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '文章类别'),
               'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '文章文章标题'),
               'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '文章文章内容'),
               'thumb' => array('name' => 'thumb', 'type' => 'string', 'require' => true, 'desc' => '文章列表页面显示缩略图'),
               'belong_type' => array('name' => 'belong_type', 'type' => 'string', 'require' => true, 'desc' => '文章所属类别'),
               'pv_max' => array('name' => 'pv_max', 'type' => 'int', 'require' => true, 'desc' => '文章最大可阅读量'),
               'read_money' => array('name' => 'read_money', 'type' => 'float', 'require' => true, 'desc' => '文章有效阅读后的金额'),
               'release_time' => array('name' => 'release_time', 'type' => 'string', 'require' => true, 'desc' => '文章发布时间'),
               'label' => array('name' => 'label', 'type' => 'string', 'require' => true, 'desc' => '标签类别，用于获取推荐阅读'),
           ),
            'createOneArticleByAdmin'=> array(
                'type'=> array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '文章类别'),
                'is_top' => array('name' => 'is_top', 'type' => 'string', 'require' => true, 'desc' => '文章置顶'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '文章文章标题'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '文章文章内容'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'require' => true, 'desc' => '文章列表页面显示缩略图'),
                'belong_type' => array('name' => 'belong_type', 'type' => 'string', 'require' => true, 'desc' => '文章所属类别'),
                'pv' => array('name' => 'pv', 'type' => 'int', 'require' => false, 'desc' => '文章默认阅读量'),
                'pv_max' => array('name' => 'pv_max', 'type' => 'int', 'require' => true, 'desc' => '文章最大可阅读量'),
                'read_money' => array('name' => 'read_money', 'type' => 'float', 'require' => true, 'desc' => '文章有效阅读后的金额'),
                'release_time' => array('name' => 'release_time', 'type' => 'string', 'require' => true, 'desc' => '文章发布时间'),
                'label' => array('name' => 'label', 'type' => 'string', 'require' => true, 'desc' => '标签类别，用于获取推荐阅读'),
            ),
            'deleteOneArticle'=> array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
            ),
            'addPv' => array(
                'id' =>array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
            ),
            'addPvMax' => array(
                 'id' =>array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
                 'qualities' =>array('name' => 'qualities', 'type' => 'int', 'require' => true, 'desc' => '文章量'),
            ),
            'editArticleById' => array(
                'id' =>array('name' => 'id', 'type' => 'int', 'require' => false, 'desc' => '文章id'),
                'type'=> array('name' => 'type', 'type' => 'int', 'require' => false, 'desc' => '文章类别'),
                'is_top' => array('name' => 'is_top', 'type' => 'string', 'require' => false, 'desc' => '文章置顶'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => false, 'desc' => '文章文章标题'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => false, 'desc' => '文章文章内容'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'require' => false, 'desc' => '文章列表页面显示缩略图'),
                'belong_type' => array('name' => 'belong_type', 'type' => 'string', 'require' => false, 'desc' => '文章所属类别'),
                'read_money' => array('name' => 'read_money', 'type' => 'float', 'require' => false, 'desc' => '文章有效阅读后的金额'),
                'release_time' => array('name' => 'release_time', 'type' => 'string', 'require' => false, 'desc' => '文章发布时间'),
                'label' => array('name' => 'label', 'type' => 'string', 'require' => false, 'desc' => '标签类别，用于获取推荐阅读'),
            ),
            'shared' => array(
                'id' =>array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '文章id'),
                'uid' =>array('name' => 'uid', 'type' => 'int', 'require' => false, 'desc' => '用户id'),
            ),
        );
    }
    /**
     * 增加咨询文章类别
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 类别id
     * @return string info.article_type_name 文章类别名称
     * @return string msg 提示信息
     */
    public function addType() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Article();
        $info = $domain->addType($this->name);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     * 获取咨询文章类别列表 或者单个类别信息
     * @desc GET 传入id查询单条信息 不传入id则显示全部类别信息
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 类别id
     * @return string info.article_type_name 文章类别名称
     * @return string msg 提示信息
     */
    public function getArticleTypesList() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Article();
        $info = $domain->getArticleTypesList($this->id);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 通过类别id来获取文章列表，并分页
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 文章id
     * @return string info.article_is_top 文章是否置顶
     * @return string info.article_belong_type 文章所属类别
     * @return string info.article_title 文章标题
     * @return string info.article_content 文章内容
     * @return string info.article_thumb 文章缩略图路径
     * @return string info.article_pv 文章有效阅读数量
     * @return string info.article_pv_max 文章最大有效阅读投放量
     * @return string info.article_readed_money 文章最有效阅读收益
     * @return string info.article_release_time 文章发布时间
     * @return string info.article_label 文章标签 用于用户推荐阅读使用
     * @return string msg 提示信息
     */

    public function getArticlesListByType(){
        $rs = array('code' => 0, 'msg' => '', 'info' =>array());

        $domain = new Domain_Article();
        $info = $domain->getArticlesListByType($this->type_id, $this->page, $this->number, $this->order, $this->by);
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 用户自己发布文章或广告（用户 自媒体）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 文章id
     * @return string info.article_is_top 文章是否置顶
     * @return string info.article_belong_type 文章所属类别
     * @return string info.article_title 文章标题
     * @return string info.article_content 文章内容
     * @return string info.article_thumb 文章缩略图路径
     * @return string info.article_pv 文章有效阅读数量
     * @return string info.article_pv_max 文章最大有效阅读投放量
     * @return string info.article_readed_money 文章最有效阅读收益
     * @return string info.article_release_time 文章发布时间
     * @return string info.article_label 文章标签 用于用户推荐阅读使用
     * @return string msg 提示信息
     */
    public function createOneArticleByUser(){
        $rs = array('code' => 0, 'msg' => '', 'info' =>array());

        $domain = new Domain_Article();
        $info = $domain->createOneArticleByUser($this->uid, $this->types, $this->title, $this->content, $this->thumb, $this->belong_type, $this->pv_max, $this->read_money, $this->release_time, $this->label );
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 管理员发布文章或广告
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 文章id
     * @return string info.article_is_top 文章是否置顶
     * @return string info.article_belong_type 文章所属类别
     * @return string info.article_title 文章标题
     * @return string info.article_content 文章内容
     * @return string info.article_thumb 文章缩略图路径
     * @return string info.article_pv 文章有效阅读数量
     * @return string info.article_pv_max 文章最大有效阅读投放量
     * @return string info.article_readed_money 文章最有效阅读收益
     * @return string info.article_release_time 文章发布时间
     * @return string info.article_label 文章标签 用于用户推荐阅读使用
     * @return string msg 提示信息
     */
    public function createOneArticleByAdmin(){
        $rs = array('code' => 0, 'msg' => '', 'info' =>array());

        $domain = new Domain_Article();
        $info = $domain->createOneArticleByAdmin($this->type, $this->is_top,  $this->title, $this->content, $this->thumb, $this->belong_type, $this->pv , $this->pv_max, $this->read_money, $this->release_time, $this->label );
        if(!empty($info)){
            $rs['info'] = $info;
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }

    /**
     * 通过id删除一篇文章
     * @desc GET
     * @return int code 操作码，0表示删除成功
     * @return string msg 提示信息
     */
    public function deleteOneArticle(){
        $rs = array('code' => 0, 'msg' => '', 'info' =>array());

        $domain = new Domain_Article();
        $result = $domain->deleteOneArticle($this->id);
        if($result){
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     * 文章有效阅读后阅读量增加
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string msg 提示信息
     */
    public function addPv(){
        $rs = array('code' => 0, 'msg' => '');
        $domain = new Domain_Article();
        $result = $domain->addPv($this->id);
        if($result){
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     * 文章加量（后台添加，用户购买，用户获赠量，用户续量）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
    public function addPvMax(){
        $rs = array('code' => 0, 'msg' => '');
        $domain = new Domain_Article();
        $result = $domain->addPvMax($this->id, $this->qualities);
        if($result){
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     * 通过文章id来修改文章（用户 管理员）
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return object info 用户信息对象
     * @return string info.id 文章id
     * @return string info.article_is_top 文章是否置顶
     * @return string info.article_belong_type 文章所属类别
     * @return string info.article_title 文章标题
     * @return string info.article_content 文章内容
     * @return string info.article_thumb 文章缩略图路径
     * @return string info.article_pv 文章有效阅读数量
     * @return string info.article_pv_max 文章最大有效阅读投放量
     * @return string info.article_readed_money 文章最有效阅读收益
     * @return string info.article_release_time 文章发布时间
     * @return string info.article_label 文章标签 用于用户推荐阅读使用
     * @return string msg 提示信息
     */
    public function editArticleById(){
        $rs = array('code' => 0, 'msg' => '');
        $domain = new Domain_Article();
        $result = $domain->editArticleById($this->id, $this->type, $this->is_top,  $this->title, $this->content, $this->thumb, $this->belong_type,  $this->read_money, $this->release_time, $this->label);
        if($result){
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
    /**
     * 文章分享成功后调用接口
     * @desc GET
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
    public function shared(){
        $rs = array('code' => 0, 'msg' => '');
        $domain = new Domain_Article();
        $result = $domain->shared($this->id, $this->uid);
        if($result){
            $rs['msg'] = 'success';
        }else{
            $rs['code'] = 1;
            $rs['msg'] = 'fail';
        }
        return $rs;
    }
}