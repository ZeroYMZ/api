<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 10:27
 */
class Model_Notice extends PhalApi_Model_NotORM
{

    public function getNotice($type, $page, $number)
    {
        return $this->getORM()
            ->select('*')
            ->where('type = ?', $type)
            ->order('id DESC')
            ->limit($page * $number - $number, $number)
            ->fetchAll();

    }

    protected function getTableName($id)
    {
        return 'new';
    }
}