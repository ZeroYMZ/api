<?php

/**评论数据模型
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 10:27
 */
class Model_Comment extends PhalApi_Model_NotORM
{
    /**通过aid获取文章评论
     * @param $aid 文章id
     * @param $page
     * @param $number
     * @return mixed
     */
    public function getComment($aid, $page, $number, $order , $by)
    {
        //排序设置默认值
        $order = $order ? $order : 'id';
        $by = $by ? $by : 'DESC';
        //返回数据
        return $this->getORM()
            ->select('*')
            ->where('aid = ?', $aid)
            ->order($order . ' ' . $by)
            ->limit($page * $number - $number, $number)
            ->fetchAll();

    }

    protected function getTableName($id)
    {
        return 'comment';
    }
}