<?php

class Model_UserInfo extends PhalApi_Model_NotORM
{

    /**通过手机获取用户信息
     * @param $user_phone
     * @return mixed
     */
    public function getUserByPhone($user_phone, $area = '*')
    {
        return $this->getORM()
            ->select($area)
            ->where('user_phone = ?', $user_phone)
            ->fetch();
    }

    /**通过uid获取好友列表
     * @param $uid 用户id
     * @param $page 页数
     * @param $number 每页显示数量
     * @return mixed
     */
    public function getUserFriend($uid, $page, $number)
    {
        return $this->getORM()
            ->select('*')
            ->where('tj_id = ?', $uid)
            ->order('id DESC')
            ->limit($page*$number-$number, $number)
            ->fetchRows();
    }

    /**用户修改密码
     * @param $user_phone
     * @param $new_password
     * @return mixed
     */
    public function changeUserPassWord($user_phone, $new_password)
    {
        $data = array('user_password' => $new_password);
        return $this->getORM()
            ->where('user_phone = ?', $user_phone)
            ->update($data);
    }

    protected function getTableName($uid)
    {
        return 'user_info';
    }
}
