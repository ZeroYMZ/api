<?php

/**咨询文章数据
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/14
 * Time: 14:50
 */
class Model_Article extends PhalApi_Model_NotORM
{
    /**通过资讯类别id获取文章列表并分页
     * @param $type_id 类别id
     * @param $page 页数
     * @param $number 显示数
     * @param $order 排序字段
     * @param $by 排序规则
     * @return mixed
     */
    public function getArticleListByTypeId($type_id, $page, $number, $order, $by)
    {
        //排序设置默认值
        $order = $order ? $order : 'article_release_time';
        $by = $by ? $by : 'DESC';
        //返回数据
        return $this->getORM()
            ->select('*')
            ->where('article_belong_type = ?', $type_id)
            ->order($order . ' ' . $by)
            ->limit($page * $number - $number, $number)
            ->fetchRows();
    }

    public function deleteMutiArticle()
    {

    }

    protected function getTableName($id)
    {
        return 'article';
    }
}