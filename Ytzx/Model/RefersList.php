<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/14
 * Time: 14:40
 */
class Model_RefersList extends PhalApi_Model_NotORM
{

    public function getRefer($uid, $type, $page, $number)
    {
        if (isset($type)) {
            return $this->getORM()
                ->select('*')
                ->where('uid = ?', $uid)
                ->and('refer_type = ?', $type)
                ->order('id DESC')
                ->limit($page * $number - $number, $number)
                ->fetchRows();
        } else {
            return $this->getORM()
                ->select('*')
                ->where('uid = ?', $uid)
                ->limit($page * $number - $number, $number)
                ->order('id DESC')
                ->fetchRows();
        }
    }

    protected function getTableName($uid)
    {
        return 'refer_list';
    }
}