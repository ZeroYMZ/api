<?php

/**广告类别数据
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/14
 * Time: 14:50
 */
class Model_ArticleType extends PhalApi_Model_NotORM
{

    public function getArticleAllTypes()
    {
        return $this->getORM()
            ->fetchRows();
    }

    protected function getTableName($id)
    {
        return 'article_types';
    }
}