<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/19
 * Time: 13:06
 */
class Model_UserTx extends PhalApi_Model_NotORM
{

    public function getUserTxByUid($uid, $page, $number)
    {
        return $this->getORM()
            ->select('*')
            ->where('uid = ?', $uid)
            ->order('id DESC')
            ->limit($page * $number - $number, $number)
            ->fetchAll();

    }

    public function getUserTxByUidAndStatus($uid, $page, $number, $status)
    {
        return $this->getORM()
            ->select('*')
            ->where('uid = ?', $uid)
            ->and('status = ?', $status)
            ->order('id DESC')
            ->limit($page * $number - $number, $number)
            ->fetchAll();

    }

    protected function getTableName($id)
    {
        return 'user_tx';
    }
}