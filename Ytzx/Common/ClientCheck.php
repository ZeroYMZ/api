<?php

class Common_ClientCheck implements PhalApi_Filter {

	public function check(){
		$uid = DI()->request->get('uid');
		if (!$uid) {
			throw new PhalApi_Exception_BadRequest("uid empty");
		}
	}
}