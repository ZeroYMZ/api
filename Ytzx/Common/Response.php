<?php

class Common_Response extends PhalApi_Response_Json {

	public function getResult(){
		if ($this->ret!=200) {
			return array(
				'error' => array(
					'code' => $this->ret,
					'type' => 'SystemException',
					'description' => $this->msg,
					)
				);
		}
		return $this->data;
	}
}