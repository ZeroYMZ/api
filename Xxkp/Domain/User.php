<?php

/**资讯用户类
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/4/18
 * Time: 16:17
 */
class Domain_User
{

    /**设置用户信息
     * @param $channel_id
     * @param $imsi
     * @param $imei
     * @param $cuid
     * @param $signature
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function login($channel_id, $imsi, $imei, $cuid, $s)
    {
        //验证签名
        if(Domain_Base::verfiySign($s)){
            //服务器当前时间
            $current_time = date("Y-m-d h:i:s",time());

            //验证是否登陆过
            $id = self::isLogined($cuid);
            if($id){
                //登陆过 更新cookie_id
                $update = array(
                    'cookie_id' => md5($current_time),
                    'logined_time' => $current_time,
                );
                $model = new Model_User();
                $result = $model->update($id, $update);
                if($result >= 1){
                    $rs = $model->get($id,'is_vip ,cookie_id');
                    $rs['client_code'] = 1;//验证成功
                    return $rs;
                }else{
                    throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
                }
            }else{
                //未登陆过 插入数据
                $data = array(
                    'id' => null,
                    'channel_id' => $channel_id,
                    'imsi' => $imsi,
                    'imei' => $imei,
                    'cuid' => $cuid,
                    'is_vip' => 0,
                    'cookie_id' => md5($current_time),
                    'logined_time' => $current_time,
                );
                $model = new Model_User();
                $id = $model->insert($data);
                if ($id) {
                    $rs = $model->get($id, 'is_vip ,cookie_id');
                    $rs['client_code'] = 1;//验证成功
                } else {
                    $rs['code'] = 402;//数据库操作失败
                }
                return $rs;
            }
        }else{
            $rs['client_code'] = 0;//验证失败
            return $rs;
        }


    }


    /**判断用户是否登陆过
     * @return bool
     */
    public function isLogined($cuid){
        $model = new Model_User();
        $rs = $model->getCuid($cuid);
        $id = $rs['id'];
        return $id;
    }
}


