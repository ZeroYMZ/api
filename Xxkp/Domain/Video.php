<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 15:03
 */
class Domain_Video
{

    /**添加视频
     * @param $category_id
     * @param $img_url
     * @param $title
     * @param $slogan
     * @param $detail
     * @param $video_url
     * @param $label
     * @return array
     * @throws PhalApi_Exception_InternalServerError
     */

    public function addVideo($category_id, $img_url, $title, $slogan, $detail, $video_url, $label)
    {
        $current_time = date('Y-m-d h:i:s', time());
        $data = array(
            'id' => null,
            'type_id' => $category_id,
            'img_url' => $img_url,
            'title' => $title,
            'slogan' => $slogan,
            'detail' => $detail,
            'video_url' => $video_url,
            'label' => $label,
            'time' => $current_time,
        );
        $model = new Model_Video();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
        } else {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }
        return $rs;
    }

    /**获取视频
     * @param $type_id
     * @param $page
     * @param $num
     * @return array
     * @throws PhalApi_Exception_InternalServerError
     */
    public function getVideo($type_id, $page, $num)
    {
        $rs = array();
        $model = new Model_Video();
        $datas = $model->getVideo($type_id, $page, $num);
        $rs['channelData'] = $datas;
        if (!$rs) {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }

        return $rs;
    }

    /**获取首页信息
     * @return array
     * @throws PhalApi_Exception_InternalServerError
     */
    public function getIndex()
    {
        $rs = array();

        $modelVideo = new Model_Video();
        $modelVtype = new Model_Vtype();

        $categorys = $modelVtype->getCategory();
        foreach ($categorys as $key => $value) {
            $category_id = $categorys[$key]['category_id'];

            $videos = $modelVideo->getVideo($category_id, 1, 6);
            $rs['channel_' . $key] = $value;
            $rs['channel_' . $key]['data'] = $videos;
        }
        if (!$rs) {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);

        }

        return $rs;
    }
}
