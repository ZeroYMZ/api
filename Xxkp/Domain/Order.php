<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 12:59
 */
class Domain_Order
{

    /**添加订单
     * @param $id
     * @return long
     * @throws PhalApi_Exception_BadRequest
     */

    public function addOrder($id,$type)
    {
        $current_time = date("Y-m-d h:i:s",time());
        $data = array(
            'id' => null,
            'uid' => $id,
            'info' => '支付成功,已变成vip用户',
            'type' => $type,
            'pay_time' => $current_time,
        );

        $model = new Model_Order();
        $oid = $model->insert($data);

        if ($oid) {
            return $oid;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入订单数据失败', 1);
        }
    }

    /**获取订单
     * @param $uid
     * @param $page
     * @param $number
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */

    public function getOrder($uid, $page, $number)
    {

        $model = new Model_Order();
        if($uid){
            $rs = $model->getOrderByUid($uid, $page, $number);
        }else{
            $rs = $model->getOrder($page, $number);
        }

        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入订单数据失败', 1);
        }
    }

}
