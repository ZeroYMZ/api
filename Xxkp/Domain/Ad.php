<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:22
 */

class Domain_Ad
{

    /**设置广告
     * @param $type
     * @param $img
     * @param $name
     * @param $content
     * @param $url
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */

    public function setAd($type, $img, $title, $detail, $url)
    {
        $info = array();

        $data = array(
            'id' => null,
            'type' => $type,
            'img' => $img,
            'title' => $title,
            'detail' => $detail,
            'url' => $url,
        );
        $model = new Model_Ad();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入广告数据失败', 1);
        }
    }

    /**获取广告
     * @param $type
     * @param $page
     * @param $number
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function getAd($type, $page, $number)
    {
        $rs = array();
        $model = new Model_Ad();
        $datas = $model->getAd($type, $page, $number);
        $rs['ad'] = $datas;
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_BadRequest('数据库插入订单数据失败', 1);
        }
    }
}
