<?php
/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:57
 */
class Domain_Vtype
{

    /**添加类型
     * @param $name
     * @param $img
     * @return array
     * @throws PhalApi_Exception_BadRequest
     */
    public function addCategory($name, $img)
    {
        $info = array();

        $data = array(
            'id' => null,
            'name' => $name,
            'img' => $img,
        );
        $model = new Model_Vtype();
        $id = $model->insert($data);
        if ($id) {
            $rs = $model->get($id);
            return $rs;
        } else {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }
    }

    /**获取所有类型
     * @return mixed
     * @throws PhalApi_Exception_BadRequest
     */
    public function getCategory()
    {
        $rs = array();
        $model = new Model_Vtype();
        $datas = $model->getCategory();
        $rs['channel'] = $datas;
        if ($rs) {
            return $rs;
        } else {
            throw new PhalApi_Exception_InternalServerError('数据库操作失败', 1);
        }
    }
}
