<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 13:22
 */
class Api_Ad extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(

            'getAd' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
                'cookie_id' => array('name' => 'cookie_id', 'type' => 'string', 'require' => false, 'desc' => '请求身份id信息'),
                'type' => array('name' => 'type', 'type' => 'int', 'require' => false, 'desc' => '广告类型，0-图片广告，1-app广告'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => false, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => false, 'desc' => '显示数量'),
                'key' => array('name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '除了key以外的值都需要封装在此'),
            ),

        );
    }


    /**
     * 通过类型获取广告
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info.status 用户支付状态
     * @return string msg 提示信息
     */
    public function getAd()
    {
        $code = 1;
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);
        $domain = new Domain_Ad();
        $info = $domain->getAd($data['type'], $data['page'], $data['num']);

        if (empty($info)) {
            $code = 0;
        }
        $info['code'] = $code;

        return $info;
    }

}