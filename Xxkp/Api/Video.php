<?php

/**
 * Created by PhpStorm.
 * User: 明哲
 * Date: 2016/5/24
 * Time: 15:03
 */
class Api_Video extends PhalApi_Api
{
    /*定制请求参数规定*/
    public function getRules()
    {
        return array(
            'getVideoByCategoryId' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
                'cookie_id' => array('name' => 'cookie_id', 'type' => 'string', 'require' => false, 'desc' => '请求身份id信息'),
                'category_id' => array('name' => 'category_id', 'type' => 'int', 'require' => false, 'desc' => '视频类型'),
                'page' => array('name' => 'page', 'type' => 'int', 'require' => false, 'desc' => '页数'),
                'num' => array('name' => 'num', 'type' => 'int', 'require' => false, 'desc' => '每页显示数量'),
                'key' => array('name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '除了key以外的值都需要封装在此'),

            ),
            'getIndex' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
                'cookie_id' => array('name' => 'cookie_id', 'type' => 'string', 'require' => false, 'desc' => '请求身份id信息'),
                'key' => array('name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '除了key以外的值都需要封装在此'),

            ),
            'getCategory' => array(
                'cuid' => array('name' => 'cuid', 'type' => 'string', 'require' => false, 'desc' => '用户唯一识别码'),
                'cookie_id' => array('name' => 'cookie_id', 'type' => 'string', 'require' => false, 'desc' => '请求身份id信息'),
                'key' => array('name' => 'key', 'type' => 'string', 'require' => true, 'desc' => '除了key以外的值都需要封装在此'),
            ),

        );
    }

    /**
     * 通过视频类型来获取视频列表并分页
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频对象
     * @return string msg 提示信息
     */
    public function getVideoByCategoryId()
    {

        $code = 1;
        $domain = new Domain_Video();
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);
        $info = $domain->getVideo($data['category_id'], $data['page'], $data['num']);

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }

    /**
     * 获取首页内容
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 种类+视频对象
     * @return string msg 提示信息
     */
    public function getIndex()
    {
        $code = 1;
        $domain = new Domain_Video();
        $data = json_decode(urldecode(DI()->des->decrypt($this->key)), true);
        $info = $domain->getIndex();

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }

    /**
     * 获取所有视频类型
     * @desc GET
     * @return int code 操作码，0表示成功，1表示用户不存在
     * @return string info 视频类型对象
     * @return string msg 提示信息
     */
    public function getCategory()
    {
        $code = 1;
        $domain = new Domain_Vtype();
        $info = $domain->getCategory();

        if (empty($info)) {
            $code = 0;
        }

        $info['code'] = $code;

        return $info;
    }


}