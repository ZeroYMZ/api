<?php

class Common_Response extends PhalApi_Response_Json
{

    public function getResult()
    {
        if ($this->ret == 200) {
            //des全部加密
            $this->data = $this->data != '' || $this->data != '' ? DI()->des->encrypt(json_encode($this->data)) : '';
            return urlencode($this->data);
            //返回正常数据
//			return $this->data;
        } else {
            return array(
//                'error' => array(
                    'code' => $this->ret,
//                    'type' => 'SystemException',
                    'description' => $this->msg,
//                )
            );
        }
    }
}