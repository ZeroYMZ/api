<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(
	/**
	 * 短信配置
	 */
    'messageConfig' => array(
		//短信平台账号
    	'msg_uid' => '304056',
		//短信平台密码
  		'msg_pwd' => 'MY304056',
		//绑定ip需要联系客服
  		'msg_ip' => '112.74.129.90',
		//绑定端口需要联系客服
  		'msg_port' => '9890',
		//平台长号由客服生成
  		'msg_srcphone' => '106900846807',
		//刷新频率 默认5min
  		'msg_refresh_second' => 360,
		//短信发送模板
  		'msg_tpl' => '【米柚科技】您的验证码是',
    ),

	/**
	 * 文章有效阅读后的提成设置
	 */
	'articleConfig' => array(
		//每篇文章分享并有效阅读默认获得2分钱
			'article_readed_money' => 0.02,
		//文章列表页数
			'article_display_number_single_page' => 5,
	),

	'rewardConfig' => array(
		'reward_money_by_register' => 1.00,
		'reward_money_by_login_in_every_day' => 1.00,
	)
);
